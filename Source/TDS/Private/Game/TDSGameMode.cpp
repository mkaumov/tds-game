// TDS Game, All Rights Reserved.


#include "Game/TDSGameMode.h"

#include "Game/TDSGameState.h"
#include "Player/TDSPlayerController.h"
#include "Player/TDSPlayerCharacter.h"
#include "Player/TDSPlayerState.h"

ATDSGameMode::ATDSGameMode() {
  GameStateClass = ATDSGameState::StaticClass();
  PlayerControllerClass = ATDSPlayerController::StaticClass();
  PlayerStateClass = ATDSPlayerState::StaticClass();
  DefaultPawnClass = ATDSPlayerCharacter::StaticClass();
}
