// TDS Game, All Rights Reserved.

#include "Player/TDSPlayerCharacter.h"

#include "EnhancedInputComponent.h"
#include "Components/Player/TDSCameraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetMathLibrary.h"

DEFINE_LOG_CATEGORY_STATIC(LogTDSPlayerCharacter, All, All);

ATDSPlayerCharacter::ATDSPlayerCharacter() {
  GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

  bUseControllerRotationPitch = false;
  bUseControllerRotationYaw = false;
  bUseControllerRotationRoll = false;

  GetCharacterMovement()->bOrientRotationToMovement = false;
  GetCharacterMovement()->RotationRate = FRotator(0.f, 360.f, 0.f);
  GetCharacterMovement()->bConstrainToPlane = true;
  GetCharacterMovement()->bSnapToPlaneAtStart = true;

  PlayerCamera = CreateDefaultSubobject<UTDSCameraComponent>(TEXT("PlayerCamera"));

  PrimaryActorTick.bCanEverTick = true;
}

void ATDSPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
  Super::SetupPlayerInputComponent(PlayerInputComponent);

  UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent);
  if (!EnhancedInputComponent) return;
}

void ATDSPlayerCharacter::BeginPlay() {
  Super::BeginPlay();

  checkf(GetCharacterMovement(), TEXT("CharacterMovement is not defined"));
  GetCharacterMovement()->MaxWalkSpeed = MovementSpeedInfo.Run;
  DestinationRotation = GetActorRotation();
}

void ATDSPlayerCharacter::Tick(float DeltaTime) {
  Super::Tick(DeltaTime);

  MovementTick(DeltaTime);
  RotationTick(DeltaTime);
}

void ATDSPlayerCharacter::MovementTick(const float DeltaTime) {
  AddMovementInput(MovementInputX, AxisX);
  AddMovementInput(MovementInputY, AxisY);

  if (IsSprinting()) {
    ToggleOrientRotationToMovement(true);
    UpdateStamina(DeltaTime, -1);
  }
  else {
    ToggleOrientRotationToMovement(false);
    if (Stamina < MaxStamina) {
      UpdateStamina(DeltaTime, 1);
    }
  }

  GEngine->AddOnScreenDebugMessage(
    1, 0.1f, FColor::Red,
    FString::Printf(TEXT("Stamina: %.2f%%"), Stamina * 100.0f),
    true
  );
}

void ATDSPlayerCharacter::RotationTick(const float DeltaTime) {
  FHitResult HitResult;
  if (!IsSprinting() && GetHitResultUnderCursor(HitResult, ECC_GameTraceChannel1)) {
    const FRotator Rotator = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location);
    DestinationRotation = FRotator{0.0f, Rotator.Yaw, 0.0f};

    const auto TargetRotation = FMath::RInterpTo(
      GetActorRotation(),
      DestinationRotation,
      DeltaTime,
      RotationRate
    );

    SetActorRotation(TargetRotation);
  }
}

void ATDSPlayerCharacter::SetMovementState(const EMovementState State) {
  MovementState = State;
  UpdateMovementSpeed();
}

void ATDSPlayerCharacter::SetMovementInputs(const float X, const float Y) {
  MovementInputX.X = X;
  MovementInputX.Y = Y;
  MovementInputY.X = -Y;
  MovementInputY.Y = X;
}

void ATDSPlayerCharacter::SetAimingEnabled(const bool AimingEnabled) {
  bAimingEnabled = AimingEnabled;
  UpdateMovementSpeed();
}

void ATDSPlayerCharacter::UpdateMovementSpeed() const {
  float ResSpeed = 0.0f;

  switch (MovementState) {
    case EMovementState::Walk:
      ResSpeed = MovementSpeedInfo.Walk;
      break;
    case EMovementState::Run:
      ResSpeed = MovementSpeedInfo.Run;
      break;
    case EMovementState::Sprint:
      ResSpeed = MovementSpeedInfo.Sprint;
      break;
  }

  if (bAimingEnabled && MovementState != EMovementState::Sprint) {
    ResSpeed *= MovementSpeedInfo.AimCoefficient;
  }

  GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDSPlayerCharacter::ToggleOrientRotationToMovement(const bool bNewOrientRotationToMovement) const {
  if (UCharacterMovementComponent* MComponent = Cast<UCharacterMovementComponent>(GetMovementComponent())) {
    MComponent->bOrientRotationToMovement = bNewOrientRotationToMovement;
  }
}

bool ATDSPlayerCharacter::GetHitResultUnderCursor(
  FHitResult& HitResult,
  const ECollisionChannel CollisionChannel
) const {
  const APlayerController* PlayerController = Cast<APlayerController>(GetController());
  if (!PlayerController) return false;

  return PlayerController->GetHitResultUnderCursor(CollisionChannel, true, HitResult);
}

void ATDSPlayerCharacter::UpdateStamina(const float DeltaSeconds, const int8 Scale) {
  if (!Scale) return;
  const auto StaminaChangeSpeed = Scale > 0 ? IncreaseStaminaValue : -DecreaseStaminaValue;
  const float NewStamina = Stamina + StaminaChangeSpeed * DeltaSeconds;
  Stamina = FMath::Min(FMath::Max(NewStamina, 0.0f), MaxStamina);

  if (Stamina <= 0.0f) {
    SetMovementState(EMovementState::Run);
  }
}

bool ATDSPlayerCharacter::CanSprint() const {
  return Stamina > 0.2f;
}
