// TDS Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "TDSGameMode.generated.h"

UCLASS()
class TDS_API ATDSGameMode : public AGameMode {
  GENERATED_BODY()

public:
  ATDSGameMode();
};
