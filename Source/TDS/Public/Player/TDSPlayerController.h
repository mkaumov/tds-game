// TDS Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "InputMappingContext.h"
#include "TDSPlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FMoveHandlerDelegate, const FInputActionInstance&, ActionInstance);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FMovementStateHandlerDelegate, const FInputActionInstance&, ActionInstance);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FMouseHandlerDelegate, const FInputActionInstance&, ActionInstance);

UCLASS()
class ATDSPlayerController : public APlayerController {
  GENERATED_BODY()

public:
  ATDSPlayerController();

  UPROPERTY(BlueprintAssignable, Category="Delegates")
  FMoveHandlerDelegate MoveHandlerDelegate;
  UPROPERTY(BlueprintAssignable, Category="Delegates")
  FMovementStateHandlerDelegate MovementStateHandlerDelegate;
  UPROPERTY(BlueprintAssignable, Category="Delegates")
  FMouseHandlerDelegate MouseHandlerDelegate;

  UFUNCTION(BlueprintNativeEvent)
  void OnMoveHandler(const FInputActionInstance& ActionInstance);
  UFUNCTION(BlueprintNativeEvent)
  void OnMovementStateHandler(const FInputActionInstance& ActionInstance);
  UFUNCTION(BlueprintNativeEvent)
  void OnMouseHandler(const FInputActionInstance& ActionInstance);

protected:
  virtual void SetupInputComponent() override;
  virtual void BeginPlay() override;

private:
  // Mapping (Context and Actions)
  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
  TObjectPtr<UInputMappingContext> MappingContext;

  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
  TObjectPtr<UInputAction> MoveAction;

  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
  TObjectPtr<UInputAction> MovementStateAction;

  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
  TObjectPtr<UInputAction> MouseAction;

  UFUNCTION()
  void OnMoveHandler_Internal(const FInputActionInstance& ActionInstance);
  UFUNCTION()
  void OnMovementStateHandler_Internal(const FInputActionInstance& ActionInstance);
  UFUNCTION()
  void OnMouseHandler_Internal(const FInputActionInstance& ActionInstance);
};
