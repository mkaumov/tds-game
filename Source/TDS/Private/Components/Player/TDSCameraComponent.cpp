// TDS Game, All Rights Reserved.

#include "Components/Player/TDSCameraComponent.h"

#include "Player/TDSPlayerCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogTDSCameraComponent, All, All);

UTDSCameraComponent::UTDSCameraComponent(const FObjectInitializer& ObjectInitializer):
  Super(ObjectInitializer) {
  SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
  SpringArmComponent->SetUsingAbsoluteRotation(true);
  SpringArmComponent->TargetArmLength = HomeHeight;
  SpringArmComponent->bDoCollisionTest = false;
  SpringArmComponent->bEnableCameraLag = true;
  SpringArmComponent->CameraLagSpeed = LagSpeed;

  CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
  CameraComponent->bUsePawnControlRotation = false;

  PrimaryComponentTick.bCanEverTick = true;
}

void UTDSCameraComponent::OnRegister() {
  Super::OnRegister();

  check(SpringArmComponent);
  check(CameraComponent);

  const auto Character = GetPlayer();
  if (!Character) return;

  SpringArmComponent->AttachToComponent(
    Character->GetRootComponent(),
    FAttachmentTransformRules::SnapToTargetIncludingScale
  );

  CameraComponent->AttachToComponent(
    SpringArmComponent,
    FAttachmentTransformRules::SnapToTargetIncludingScale,
    USpringArmComponent::SocketName
  );

  SpringArmComponent->SetRelativeRotation(HomeRotation);
}

void UTDSCameraComponent::BeginPlay() {
  Super::BeginPlay();

  if (const auto Character = GetPlayer()) {
    HomeRotation.Yaw = Character->GetActorRotation().Yaw;
    GoHome();
  }
}

void UTDSCameraComponent::TickComponent(
  float DeltaTime,
  ELevelTick TickType,
  FActorComponentTickFunction* ThisTickFunction
) {
  Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

  if (!DestinationRotation.Equals(SpringArmComponent->GetRelativeRotation())) {
    SpringArmComponent->SetRelativeRotation(FMath::RInterpTo(
      SpringArmComponent->GetRelativeRotation(),
      DestinationRotation,
      DeltaTime,
      RotationRate
    ));

    SetPlayerMovementInputs();
  }
  
  if (SpringArmComponent->TargetArmLength != DestinationHeight) {
    SpringArmComponent->TargetArmLength = FMath::FInterpTo(
      SpringArmComponent->TargetArmLength,
      DestinationHeight,
      DeltaTime,
      ChangeHeightRate
    );
  }
}

void UTDSCameraComponent::Zoom(const float Value) {  
  if (!Value) return;

  DestinationHeight = FMath::Clamp(
    SpringArmComponent->TargetArmLength + ChangeHeightStep * -Value,
    MinHeight,
    MaxHeight
  );
}

void UTDSCameraComponent::Rotate(const float XDirection, const float YDirection) {
  if (bRotationPitchEnable && !FMath::IsNearlyZero(FMath::Abs(YDirection), 0.1)) {
    DestinationRotation.Pitch = FMath::ClampAngle(
      DestinationRotation.Pitch + RotationStepPitch * YDirection,
      -80.0f, -65.0f
    );
  }

  if (!FMath::IsNearlyZero(FMath::Abs(XDirection), 0.01)) {
    GEngine->AddOnScreenDebugMessage(10, 2, FColor::Blue, FString::Printf(TEXT("%.3f"), XDirection));
    DestinationRotation.Yaw = DestinationRotation.Yaw + RotationStepYaw * XDirection;
  }
}

void UTDSCameraComponent::GoHome() {
  DestinationHeight = HomeHeight;
  DestinationRotation = HomeRotation;
}

void UTDSCameraComponent::Shake() {
  if (ShakeFX) {
    UGameplayStatics::PlayWorldCameraShake(this, ShakeFX, CameraComponent->GetComponentLocation(), 0.0f, 30.0f, 1.0f);
  }
}

void UTDSCameraComponent::SetPlayerMovementInputs() const {
  if (const auto Character = GetPlayer()) {
    const float Angle = FMath::DegreesToRadians(SpringArmComponent->GetRelativeRotation().Yaw);
    Character->SetMovementInputs(FMath::Cos(Angle), FMath::Sin(Angle));
  }
}

ATDSPlayerCharacter* UTDSCameraComponent::GetPlayer() const {
  return GetOwner<ATDSPlayerCharacter>();
}
