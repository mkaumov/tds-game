// TDS Game, All Rights Reserved.

using UnrealBuildTool;

public class TDS : ModuleRules
{
	public TDS(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[]
		{
			"Core",
			"CoreUObject",
			"Engine",
			"InputCore",
			"EnhancedInput",
			"Niagara",
			"AIModule",
			// "NavigationSystem",
			// "HeadMountedDisplay"
		});

		PublicIncludePaths.AddRange(new string[]
		{
			"TDS/Public/Game",
			"TDS/Public/Player",
			"TDS/Public/Libraries",
			"TDS/Public/Components",
		});
	}
}