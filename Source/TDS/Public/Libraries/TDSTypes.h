﻿// TDS Game, All Rights Reserved.

#pragma once

#include "TDSTypes.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8 {
  Walk UMETA(DisplayName = "Walk"),
  Run UMETA(DisplayName = "Run"),
  Sprint UMETA(DisplayName = "Sprint"),
};

USTRUCT(BlueprintType)
struct FCharacterSpeed {
  GENERATED_BODY()

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MovementSpeed")
  float Walk = 200.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MovementSpeed")
  float Run = 400.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MovementSpeed")
  float Sprint = 600.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MovementSpeed", meta=(ClampMax = 1.0f, ClampMin = 0.3f))
  float AimCoefficient = 0.7f;
};
