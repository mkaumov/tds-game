// TDS Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "TDSGameState.generated.h"

UCLASS()
class TDS_API ATDSGameState : public AGameState {
  GENERATED_BODY()
};
