// TDS Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "TDSPlayerState.generated.h"

UCLASS()
class TDS_API ATDSPlayerState : public APlayerState {
  GENERATED_BODY()
};
