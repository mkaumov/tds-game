// TDS Game, All Rights Reserved.

#include "Player/TDSPlayerController.h"

#include "Components/Player/TDSCameraComponent.h"
#include "Player/TDSPlayerCharacter.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"

DEFINE_LOG_CATEGORY_STATIC(LogTDSPlayerController, All, All);

ATDSPlayerController::ATDSPlayerController() {
  bShowMouseCursor = true;
  DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ATDSPlayerController::BeginPlay() {
  Super::BeginPlay();

  checkf(MappingContext, TEXT("MappingContext must be set"));

  UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(
    GetLocalPlayer()
  );
  if (!Subsystem) return;

  Subsystem->AddMappingContext(MappingContext, 0);
}

void ATDSPlayerController::SetupInputComponent() {
  Super::SetupInputComponent();

  const auto EnhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent);
  if (!EnhancedInputComponent) return;

  checkf(MoveAction, TEXT("MoveAction must be set"));
  checkf(MovementStateAction, TEXT("MovementStateAction must be set"));
  checkf(MouseAction, TEXT("MouseAction must be set"));

  EnhancedInputComponent->BindAction(
    MoveAction,
    ETriggerEvent::Triggered,
    this,
    &ThisClass::OnMoveHandler_Internal
  );
  EnhancedInputComponent->BindAction(
    MovementStateAction,
    ETriggerEvent::Triggered,
    this,
    &ThisClass::OnMovementStateHandler_Internal
  );
  EnhancedInputComponent->BindAction(
    MouseAction,
    ETriggerEvent::Triggered,
    this,
    &ThisClass::OnMouseHandler_Internal
  );
}

void ATDSPlayerController::OnMoveHandler_Internal(const FInputActionInstance& ActionInstance) {
  OnMoveHandler(ActionInstance);
  MoveHandlerDelegate.Broadcast(ActionInstance);
}

void ATDSPlayerController::OnMovementStateHandler_Internal(const FInputActionInstance& ActionInstance) {
  OnMovementStateHandler(ActionInstance);
  MovementStateHandlerDelegate.Broadcast(ActionInstance);
}

void ATDSPlayerController::OnMouseHandler_Internal(const FInputActionInstance& ActionInstance) {
  OnMouseHandler(ActionInstance);
  MouseHandlerDelegate.Broadcast(ActionInstance);
}

void ATDSPlayerController::OnMoveHandler_Implementation(const FInputActionInstance& ActionInstance) {
  if (ATDSPlayerCharacter* TDSCharacter = Cast<ATDSPlayerCharacter>(GetCharacter())) {
    const auto Vector = ActionInstance.GetValue().Get<FVector2D>();

    TDSCharacter->SetAxisX(Vector.X);
    TDSCharacter->SetAxisY(Vector.Y);
  }
}

void ATDSPlayerController::OnMovementStateHandler_Implementation(const FInputActionInstance& ActionInstance) {
  ATDSPlayerCharacter* TDSCharacter = Cast<ATDSPlayerCharacter>(GetCharacter());
  if (!TDSCharacter) return;

  const auto Vector = ActionInstance.GetValue().Get<FVector2D>();

  static EMovementState PrevMovementState;

  if (Vector.X && TDSCharacter->GetMovementState() != EMovementState::Sprint && TDSCharacter->CanSprint()) {
    PrevMovementState = TDSCharacter->GetMovementState();
    TDSCharacter->SetMovementState(EMovementState::Sprint);
  }
  else if (Vector.Y && !Vector.X) {
    TDSCharacter->SetMovementState(
      TDSCharacter->GetMovementState() == EMovementState::Walk ? EMovementState::Run : EMovementState::Walk
    );
  }
  else if (!Vector.Y && !Vector.X) {
    TDSCharacter->SetMovementState(PrevMovementState);
  }
}

void ATDSPlayerController::OnMouseHandler_Implementation(const FInputActionInstance& ActionInstance) {
  ATDSPlayerCharacter* TDSCharacter = Cast<ATDSPlayerCharacter>(GetCharacter());
  if (!TDSCharacter) return;

  const auto PlayerCamera = TDSCharacter->GetPlayerCamera();
  if (!PlayerCamera) return;

  const auto Vector = ActionInstance.GetValue().Get<FVector>();

  TDSCharacter->SetAimingEnabled(Vector.Y != 0);
  PlayerCamera->Zoom(Vector.Z);

  if (Vector.X) {
    float MouseX, MouseY;
    GetInputMouseDelta(MouseX, MouseY);
    PlayerCamera->Rotate(MouseX, MouseY);
  }
}
