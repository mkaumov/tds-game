// TDS Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Libraries/TDSTypes.h"
#include "GameFramework/Character.h"
#include "GameFramework/PawnMovementComponent.h"
#include "TDSPlayerCharacter.generated.h"

class UTDSCameraComponent;

// todo: create TDSCharacter, inherit from it and move common logic to it

UCLASS()
class TDS_API ATDSPlayerCharacter : public ACharacter {
  GENERATED_BODY()

public:
  ATDSPlayerCharacter();

  virtual void Tick(float DeltaTime) override;
  virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

  // Aiming and firing
  void SetAimingEnabled(const bool AimingEnabled);
  
  // Movement
  void SetAxisX(const float Value) { AxisX = Value; }
  void SetAxisY(const float Value) { AxisY = Value; }
  void SetMovementState(const EMovementState State);
  void SetMovementInputs(const float X, const float Y);
  bool CanSprint() const;
  EMovementState GetMovementState() const { return MovementState; }

  // Camera
  UTDSCameraComponent* GetPlayerCamera() const { return PlayerCamera; }

protected:
  virtual void BeginPlay() override;

private:
  // Health and Stamina // todo: move to health component
  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stamina", meta = (AllowPrivateAccess = "true"))
  float MaxStamina = 1.0f;
  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stamina", meta = (AllowPrivateAccess = "true"))
  float Stamina = 1.0f;
  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stamina", meta = (AllowPrivateAccess = "true", ClampMin=0.001f, ClampMax=0.5))
  float IncreaseStaminaValue = 0.06f; // in second
  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stamina", meta = (AllowPrivateAccess = "true", ClampMin=0.001f, ClampMax=0.5))
  float DecreaseStaminaValue = 0.12f; // in second

  void UpdateStamina(const float DeltaSeconds, const int8 Scale);
  
  // Aiming and firing
  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Fire", meta = (AllowPrivateAccess = "true"))
  bool bAimingEnabled = false;
  
  // Movement
  float AxisX = 0.0f;
  float AxisY = 0.0f;
  FVector MovementInputX{1.0f, 0.0f, 0.0f};
  FVector MovementInputY{0.0f, 1.0f, 0.0f};
  FRotator DestinationRotation;
  UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
  EMovementState MovementState = EMovementState::Run;
  UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
  FCharacterSpeed MovementSpeedInfo;
  UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
  float RotationRate = 9.0f;

  void MovementTick(const float DeltaTime);
  void RotationTick(const float DeltaTime);
  void UpdateMovementSpeed() const;
  void ToggleOrientRotationToMovement(const bool bNewOrientRotationToMovement) const;
  bool IsSprinting() const { return MovementState == EMovementState::Sprint; }

  // Camera
  UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
  TObjectPtr<UTDSCameraComponent> PlayerCamera;

  bool GetHitResultUnderCursor(FHitResult& HitResult, const ECollisionChannel CollisionChannel) const;
};
