// TDS Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDSCameraComponent.generated.h"

UCLASS(BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TDS_API UTDSCameraComponent : public UActorComponent {
  GENERATED_BODY()

public:
  UTDSCameraComponent(const FObjectInitializer& ObjectInitializer);

  virtual void TickComponent(
    float DeltaTime,
    ELevelTick TickType,
    FActorComponentTickFunction* ThisTickFunction
  ) override;

  UFUNCTION(BlueprintCallable)
  void Zoom(const float Value);
  UFUNCTION(BlueprintCallable)
  void Rotate(const float XDirection, const float YDirection);
  UFUNCTION(BlueprintCallable)
  void GoHome();
  UFUNCTION(BlueprintCallable)
  void Shake();

protected:
  virtual void BeginPlay() override;
  virtual void OnRegister() override;

private:
  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Player Camera Components", meta=(AllowPrivateAccess = "true"))
  TObjectPtr<class UCameraComponent> CameraComponent;
  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Player Camera Components", meta=(AllowPrivateAccess = "true"))
  TObjectPtr<class USpringArmComponent> SpringArmComponent;

  // Settings.Height
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Player Camera Settings", meta=(AllowPrivateAccess = "true"))
  float HomeHeight = 1300.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Player Camera Settings", meta=(AllowPrivateAccess = "true"))
  float MinHeight = 800.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Player Camera Settings", meta=(AllowPrivateAccess = "true"))
  float MaxHeight = 2000.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Player Camera Settings", meta=(AllowPrivateAccess = "true"))
  float ChangeHeightStep = 100.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Player Camera Settings", meta=(AllowPrivateAccess = "true"))
  float ChangeHeightRate = 25.0f;
  // Settings.Lag
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Player Camera Settings", meta=(AllowPrivateAccess = "true"))
  int32 LagSpeed = 5;
  // Settings.Rotation
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Player Camera Settings", meta=(AllowPrivateAccess = "true"))
  FRotator HomeRotation{-73.0f, 0.0f, 0.0f};
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Player Camera Settings", meta=(AllowPrivateAccess = "true"))
  float RotationStepPitch = 10.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Player Camera Settings", meta=(AllowPrivateAccess = "true"))
  float RotationStepYaw = 10.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Player Camera Settings", meta=(AllowPrivateAccess = "true"))
  float RotationRate = 8.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Player Camera Settings", meta=(AllowPrivateAccess = "true"))
  bool bRotationPitchEnable = false;
  // Settings.Shake
  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Player Camera Settings", meta=(AllowPrivateAccess = "true"))
  TSubclassOf<UCameraShakeBase> ShakeFX;

  float DestinationHeight;
  FRotator DestinationRotation;

  void SetPlayerMovementInputs() const;
  class ATDSPlayerCharacter* GetPlayer() const;
};
